<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Contracts;

interface NoCaptchaManager
{
    public function version(?string $version = null): NoCaptcha;
}
