<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Contracts;

interface CaptchaRequest
{
    public function send(string $url, bool $curled = true): string;
}
