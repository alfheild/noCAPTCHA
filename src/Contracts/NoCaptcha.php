<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Contracts;

use Arcanedev\NoCaptcha\Utilities\AbstractResponse;
use Illuminate\Support\HtmlString;

interface NoCaptcha
{
    public function setRequestClient(CaptchaRequest $request): self;

    public function setLang(string $lang): self;

    public function getLang(): ?string;

    public function verify(string $response, ?string $clientIp = null): AbstractResponse;

    public function script(): HtmlString;

    public function getApiScript(): HtmlString;
}
