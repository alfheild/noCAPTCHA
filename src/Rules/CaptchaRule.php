<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Arr;

class CaptchaRule implements ValidationRule
{
    protected ?string $version;

    protected array $skipIps = [];

    public function __construct(?string $version = null)
    {
        $this->setVersion($version);
        $this->skipIps(config()->get('no-captcha.skip-ips', []));
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function skipIps(array|string $ip): self
    {
        $this->skipIps = Arr::wrap($ip);

        return $this;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $ip = request()->ip();

        if (in_array($ip, $this->skipIps, true)) {
            return;
        }

        $verified = no_captcha($this->version)
            ->verify($value, $ip)
            ->isSuccessful();
        if (!$verified) {
            $fail(trans('validation.captcha'));
        }
    }
}
