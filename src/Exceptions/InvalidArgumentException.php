<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Exceptions;

class InvalidArgumentException extends NoCaptchaException {}
