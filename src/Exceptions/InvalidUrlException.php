<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Exceptions;

class InvalidUrlException extends NoCaptchaException {}
