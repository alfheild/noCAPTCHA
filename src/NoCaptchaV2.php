<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha;

use Arcanedev\NoCaptcha\Elements\Div;
use Arcanedev\NoCaptcha\Exceptions\InvalidArgumentException;
use Arcanedev\NoCaptcha\Utilities\AbstractResponse;
use Arcanedev\NoCaptcha\Utilities\ResponseV2;
use Illuminate\Support\Arr;
use Illuminate\Support\HtmlString;

class NoCaptchaV2 extends AbstractNoCaptcha
{
    protected bool $scriptLoaded = false;

    private function getScriptSrc(?string $callbackName = null): string
    {
        $queries = [];

        if ($this->hasLang()) {
            Arr::set($queries, 'hl', $this->getLang());
        }

        if ($this->hasCallbackName($callbackName)) {
            Arr::set($queries, 'onload', $callbackName);
            Arr::set($queries, 'render', 'explicit');
        }

        return self::getClientUrl() . (count($queries) ? '?' . http_build_query($queries) : '');
    }

    public function display(string $name = null, array $attributes = []): Div
    {
        if ($name === AbstractNoCaptcha::CAPTCHA_NAME) {
            throw new InvalidArgumentException(
                'The captcha name must be different from "' . AbstractNoCaptcha::CAPTCHA_NAME . '".',
            );
        }

        return Div::create()
            ->setAttribute('id', $name)
            ->setAttribute('name', $name)
            ->addAttributes($this->prepareAttributes($attributes));
    }

    public function script(?string $callbackName = null): HtmlString
    {
        $script = '';

        if (!$this->scriptLoaded) {
            $script = '<script src="' . $this->getScriptSrc($callbackName) . '" async defer></script>';
            $this->scriptLoaded = true;
        }

        return new HtmlString($script);
    }

    public function getApiScript(): HtmlString
    {
        $script = <<<'HTML'
<script>
    window.noCaptcha = {
        captchas: [],
        reset: function(name) {
            var captcha = window.noCaptcha.get(name);

            if (captcha)
                window.noCaptcha.resetById(captcha.id);
        },
        resetById: function(id) {
            grecaptcha.reset(id);
        },
        get: function(name) {
            return window.noCaptcha.find(function (captcha) {
                return captcha.name === name;
            });
        },
        getById: function(id) {
            return window.noCaptcha.find(function (captcha) {
                return captcha.id === id;
            });
        },
        find: function(callback) {
            return window.noCaptcha.captchas.find(callback);
        },
        render: function(name, sitekey) {
            var captcha = {
                id: grecaptcha.render(name, {'sitekey' : sitekey}),
                name: name,
            };

            window.noCaptcha.captchas.push(captcha);

            return captcha;
        },
    };
</script>
HTML;

        return new HtmlString($script);
    }

    public function scriptWithCallback(
        array $captchas,
        string $callbackName = 'captchaRenderCallback',
    ): HtmlString {
        $script = $this->script($callbackName)->toHtml();

        if (!empty($script) && !empty($captchas)) {
            $script = implode(PHP_EOL, [
                $this->getApiScript()->toHtml(),
                '<script>',
                "var $callbackName = function() {",
                $this->renderCaptchas($captchas),
                '};',
                '</script>',
                $script,
            ]);
        }

        return new HtmlString($script);
    }

    private function renderCaptchas(array $captchas): string
    {
        return implode(PHP_EOL, array_map(function ($captcha) {
            return "if (document.getElementById('{$captcha}')) { window.noCaptcha.render('{$captcha}', '{$this->siteKey}'); }";
        }, $captchas));
    }

    private function hasCallbackName(?string $callbackName): bool
    {
        return !(is_null($callbackName) || trim($callbackName) === '');
    }

    protected function parseResponse(string $json): AbstractResponse
    {
        return ResponseV2::fromJson($json);
    }

    private function prepareAttributes(array $attributes): array
    {
        $attributes = array_merge(
            ['class' => 'g-recaptcha', 'data-sitekey' => $this->siteKey],
            array_filter($attributes),
        );

        $this->checkDataAttribute($attributes, 'data-type', ['image', 'audio'], 'image');
        $this->checkDataAttribute($attributes, 'data-theme', ['light', 'dark'], 'light');
        $this->checkDataAttribute($attributes, 'data-size', ['normal', 'compact', 'invisible'], 'normal');
        $this->checkDataAttribute($attributes, 'data-badge', ['bottomright', 'bottomleft', 'inline'], 'bottomright');

        return $attributes;
    }

    private function checkDataAttribute(
        array &$attributes,
        string $name,
        array $supported,
        string $default,
    ): void
    {
        $attribute = $attributes[$name] ?? null;

        if (!is_null($attribute)) {
            $attribute = (is_string($attribute) && in_array($attribute, $supported, true))
                ? strtolower(trim($attribute))
                : $default;

            $attributes[$name] = $attribute;
        }
    }
}
