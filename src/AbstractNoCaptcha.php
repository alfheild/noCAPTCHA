<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha;

use Arcanedev\NoCaptcha\Contracts\CaptchaRequest as CaptchaRequestInterface;
use Arcanedev\NoCaptcha\Utilities\AbstractResponse;
use Arcanedev\NoCaptcha\Utilities\CaptchaRequest;
use Psr\Http\Message\ServerRequestInterface;

abstract class AbstractNoCaptcha implements Contracts\NoCaptcha
{
    public const CAPTCHA_NAME = 'g-recaptcha-response';
    protected string $secretKey;
    protected string $siteKey;
    protected ?string $lang;
    protected Contracts\CaptchaRequest $request;
    public static bool $useGlobalDomain = false;

    public function __construct(
        string $secret,
        string $siteKey,
        ?string $lang = null,
    ) {
        $this->setSecretKey($secret);
        $this->setSiteKey($siteKey);
        $this->setLang($lang);

        $this->setRequestClient(new CaptchaRequest());
    }

    protected function setSecretKey(string $secretKey): self
    {
        $this->checkKey('secret key', $secretKey);

        $this->secretKey = $secretKey;

        return $this;
    }

    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    protected function setSiteKey(string $siteKey): static
    {
        $this->checkKey('site key', $siteKey);

        $this->siteKey = $siteKey;

        return $this;
    }

    public function setLang(?string $lang): static
    {
        $this->lang = $lang;

        return $this;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setRequestClient(CaptchaRequestInterface $request): self
    {
        $this->request = $request;

        return $this;
    }

    public static function getClientUrl(): string
    {
        return static::$useGlobalDomain
            ? 'https://www.recaptcha.net/recaptcha/api.js'
            : 'https://www.google.com/recaptcha/api.js';
    }

    public static function getVerificationUrl(): string
    {
        return static::$useGlobalDomain
            ? 'https://www.recaptcha.net/recaptcha/api/siteverify'
            : 'https://www.google.com/recaptcha/api/siteverify';
    }

    public function verify(string|bool $response, ?string $clientIp = null): AbstractResponse
    {
        return $this->sendVerifyRequest([
            'secret' => $this->secretKey,
            'response' => $response,
            'remoteip' => $clientIp,
        ]);
    }

    public function verifyRequest(ServerRequestInterface $request): AbstractResponse
    {
        $body = $request->getParsedBody();
        $server = $request->getServerParams();

        return $this->verify(
            $body[self::CAPTCHA_NAME] ?? '',
            $server['REMOTE_ADDR'] ?? null,
        );
    }

    protected function sendVerifyRequest(array $query = []): AbstractResponse
    {
        $json = $this->request->send(
            self::getVerificationUrl() . '?' . http_build_query(array_filter($query)),
        );

        return $this->parseResponse($json);
    }

    abstract protected function parseResponse(string $json): AbstractResponse;

    protected function hasLang(): bool
    {
        return !empty($this->getLang());
    }

    private function checkKey(string $name, string &$value): void
    {
        $value = trim($value);

        if (empty($value)) {
            throw new Exceptions\ApiException("Config {$name} must not be empty");
        }
    }
}
