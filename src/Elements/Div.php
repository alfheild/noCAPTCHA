<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Elements;

class Div extends BaseElement
{
    protected string $tag = 'div';
}
