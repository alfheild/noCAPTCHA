<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Elements;

use Arcanedev\NoCaptcha\Elements\BaseElement;

class Input extends BaseElement
{
    protected string $tag = 'input';
}
