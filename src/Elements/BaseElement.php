<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Elements;

use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;

abstract class BaseElement implements Htmlable
{
    protected string $tag;
    protected Collection $attributes;

    public function __construct()
    {
        $this->attributes = new Collection();
    }

    public static function create(): static
    {
        return new static();
    }

    public function setAttribute(string $attribute, ?string $value = null): self
    {
        $this->attributes->put($attribute, $value);

        return $this;
    }

    public function addAttributes(iterable $attributes): self
    {
        foreach ($attributes as $attribute => $value) {
            $this->setAttribute($attribute, $value);
        }

        return $this;
    }

    public function open(): HtmlString
    {
        $tag = $this->attributes->isEmpty()
            ? "<{$this->tag}>"
            : "<{$this->tag} {$this->renderAttributes()}>";

        return new HtmlString($tag);
    }

    public function close(): HtmlString
    {
        return new HtmlString(
            $this->isVoidElement()
                ? ''
                : "</{$this->tag}>"
        );
    }

    public function render(): HtmlString
    {
        return new HtmlString(
            $this->open() . $this->close(),
        );
    }

    public function __toString(): string
    {
        return $this->render()->toHtml();
    }

    public function toHtml(): string
    {
        return $this->render()->toHtml();
    }

    protected function renderAttributes(): string
    {
        if ($this->attributes->isEmpty()) {
            return '';
        }

        $attributeStrings = [];

        foreach ($this->attributes->toArray() as $attribute => $value) {
            if ($value === null) {
                continue;
            }
            if ($value === '') {
                $attributeStrings[] = $attribute;

                continue;
            }

            $value = htmlentities($value, ENT_QUOTES, 'UTF-8', false);

            $attributeStrings[] = "{$attribute}=\"{$value}\"";
        }

        return implode(' ', $attributeStrings);
    }

    protected function isVoidElement(): bool
    {
        return in_array($this->tag, [
            'area', 'base', 'br', 'col', 'embed', 'hr',
            'img', 'input', 'keygen', 'link', 'menuitem',
            'meta', 'param', 'source', 'track', 'wbr',
        ]);
    }
}
