<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha;

use Arcanedev\NoCaptcha\Contracts\NoCaptcha as NoCaptchaContract;
use Arcanedev\NoCaptcha\Contracts\NoCaptchaManager as NoCaptchaManagerContract;
use Illuminate\Support\Manager;

class NoCaptchaManager extends Manager implements NoCaptchaManagerContract
{
    public function getDefaultDriver(): string
    {
        return $this->config('version');
    }

    public function version(string $version = null): NoCaptchaContract
    {
        return $this->driver($version);
    }

    public function createV2Driver(): NoCaptchaContract|NoCaptchaV2
    {
        return $this->buildDriver(NoCaptchaV2::class);
    }

    public function createV3Driver(): NoCaptchaContract|NoCaptchaV3
    {
        return $this->buildDriver(NoCaptchaV3::class);
    }

    protected function buildDriver(string $driver): NoCaptchaContract
    {
        return $this->container->make($driver, [
            'secret'  => $this->config('secret'),
            'siteKey' => $this->config('sitekey'),
            'lang'    => $this->config('lang') ?: $this->container->getLocale(),
        ]);
    }

    protected function config(string $key = '')
    {
        return $this->config->get("no-captcha.{$key}");
    }
}
