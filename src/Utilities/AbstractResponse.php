<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Utilities;

use Illuminate\Contracts\Support\{Arrayable, Jsonable};
use JsonSerializable;

abstract class AbstractResponse implements Arrayable, Jsonable, JsonSerializable
{
    /*
     * Invalid JSON received
     */
    public const E_INVALID_JSON = 'invalid-json';
    /*
     * Did not receive a 200 from the service
     */
    public const E_BAD_RESPONSE = 'bad-response';
    /*
     * ReCAPTCHA response not provided
     */
    public const E_MISSING_INPUT_RESPONSE = 'missing-input-response';
    /**
     * Not a success, but no error codes received!
     */
    public const E_UNKNOWN_ERROR = 'unknown-error';
    protected bool $success = false;
    protected array $errorCodes = [];
    /*
     * The hostname of the site where the reCAPTCHA was solved.
     */
    protected ?string $hostname;
    /*
     * Timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
     */
    protected ?string $challengeTs;
    protected ?string $apkPackageName;

    public function __construct(
        bool $success,
        array $errorCodes = [],
        ?string $hostname = null,
        ?string $challengeTs = null,
        ?string $apkPackageName = null,
    ) {
        $this->success = $success;
        $this->errorCodes = $errorCodes;
        $this->hostname = $hostname;
        $this->challengeTs = $challengeTs;
        $this->apkPackageName = $apkPackageName;
    }

    public function getErrorCodes(): array
    {
        return $this->errorCodes;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function getChallengeTs(): ?string
    {
        return $this->challengeTs;
    }

    public function getApkPackageName(): ?string
    {
        return $this->apkPackageName;
    }

    public static function fromJson(string $json): self
    {
        $responseData = json_decode($json, true);

        if (!$responseData) {
            return new static(false, [static::E_INVALID_JSON]);
        }

        return static::fromArray($responseData);
    }

    abstract public static function fromArray(array $array): AbstractResponse;

    abstract public function toArray(): array;

    public function toJson($options = 0): string
    {
        return json_encode($this->jsonSerialize(), JSON_THROW_ON_ERROR | $options);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function isSuccessful(): bool
    {
        return $this->success === true;
    }

    public function isHostname(string $hostname): bool
    {
        return $this->getHostname() === $hostname;
    }
}
