<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Utilities;

class ResponseV2 extends AbstractResponse
{
    public static function fromArray(array $array): ResponseV2
    {
        $hostname = $array['hostname'] ?? null;
        $challengeTs = $array['challenge_ts'] ?? null;
        $apkPackageName = $array['apk_package_name'] ?? null;

        if (isset($array['success']) && $array['success']) {
            return new static(true, [], $hostname, $challengeTs, $apkPackageName);
        }

        if (!(isset($array['error-codes']) && is_array($array['error-codes']))) {
            $array['error-codes'] = [static::E_UNKNOWN_ERROR];
        }

        return new static(
            false,
            $array['error-codes'],
            $hostname,
            $challengeTs,
            $apkPackageName,
        );
    }

    public function toArray(): array
    {
        return [
            'success' => $this->isSuccessful(),
            'hostname' => $this->getHostname(),
            'challenge_ts' => $this->getChallengeTs(),
            'apk_package_name' => $this->getApkPackageName(),
            'error-codes' => $this->getErrorCodes(),
        ];
    }
}
