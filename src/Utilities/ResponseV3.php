<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Utilities;

class ResponseV3 extends AbstractResponse
{
    /*
     * Could not connect to service
     */
    public const E_CONNECTION_FAILED = 'connection-failed';
    /*
     * Expected hostname did not match
     */
    public const E_HOSTNAME_MISMATCH = 'hostname-mismatch';
    /*
     * Expected APK package name did not match
     */
    public const E_APK_PACKAGE_NAME_MISMATCH = 'apk_package_name-mismatch';
    /*
     * Expected action did not match
     */
    public const E_ACTION_MISMATCH = 'action-mismatch';
    /*
     * Score threshold not met
     */
    public const E_SCORE_THRESHOLD_NOT_MET = 'score-threshold-not-met';
    /*
     * Challenge timeout
     */
    public const E_CHALLENGE_TIMEOUT = 'challenge-timeout';
    private ?float $score;
    private ?string $action;

    public function __construct(
        bool $success,
        array $errorCodes = [],
        ?string $hostname = null,
        ?string $challengeTs = null,
        ?string $apkPackageName = null,
        ?float $score = null,
        ?string $action = null,
    ) {
        parent::__construct(
            $success,
            $errorCodes,
            $hostname,
            $challengeTs,
            $apkPackageName,
        );

        $this->score = $score;
        $this->action = $action;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public static function fromArray(array $array): ResponseV3
    {
        $hostname = $array['hostname'] ?? null;
        $challengeTs = $array['challenge_ts'] ?? null;
        $apkPackageName = $array['apk_package_name'] ?? null;
        $score = isset($array['score']) ? (float) $array['score'] : null;
        $action = $array['action'] ?? null;

        if (isset($array['success']) && $array['success']) {
            return new static(
                true,
                [],
                $hostname,
                $challengeTs,
                $apkPackageName,
                $score,
                $action
            );
        }

        if (!(isset($array['error-codes']) && is_array($array['error-codes']))) {
            $array['error-codes'] = [static::E_UNKNOWN_ERROR];
        }

        return new static(
            false,
            $array['error-codes'],
            $hostname,
            $challengeTs,
            $apkPackageName,
            $score,
            $action
        );
    }

    public function toArray(): array
    {
        return [
            'success' => $this->isSuccessful(),
            'hostname' => $this->getHostname(),
            'challenge_ts' => $this->getChallengeTs(),
            'apk_package_name' => $this->getApkPackageName(),
            'score' => $this->getScore(),
            'action' => $this->getAction(),
            'error-codes' => $this->getErrorCodes(),
        ];
    }
}
