<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Utilities;

use Arcanedev\NoCaptcha\Contracts\CaptchaRequest as RequestContract;
use Arcanedev\NoCaptcha\Exceptions\InvalidUrlException;

class CaptchaRequest implements RequestContract
{
    protected string $url = '';

    protected function setUrl(string $url): self
    {
        $this->checkUrl($url);

        $this->url = $url;

        return $this;
    }

    protected function sendWithCurl(): string|bool
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    public function send(string $url, bool $curled = true): string
    {
        $this->setUrl($url);

        if ($curled && $this->curlIsAvailable()) {
            $result = $this->sendWithCurl();
        } else {
            $result = file_get_contents($this->url);
        }

        return $this->isValidResult($result) ? $result : '{}';
    }

    private function checkUrl(string &$url): void
    {
        $url = trim($url);

        if (empty($url)) {
            throw new InvalidUrlException('The url must not be empty');
        }

        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new InvalidUrlException("The url [{$url}] is invalid");
        }
    }

    private function curlIsAvailable(): bool
    {
        return function_exists('curl_version');
    }

    private function isValidResult(mixed $result): bool
    {
        return is_string($result) && !empty($result);
    }
}
