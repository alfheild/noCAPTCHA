<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha;

use Arcanedev\NoCaptcha\Contracts\NoCaptcha as NoCaptchaContract;
use Arcanedev\NoCaptcha\Contracts\NoCaptchaManager as NoCaptchaManagerContract;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class NoCaptchaServiceProvider extends ServiceProvider implements DeferrableProvider
{
    protected string $package = 'no-captcha';

    public function register(): void
    {
        parent::register();

        $this->mergeConfigFrom(
            __DIR__ . "/../config/{$this->package}.php",
            $this->package,
        );
        $this->registerNoCaptchaManager();
    }

    public function boot(): void
    {
        $this->publishes([
            __DIR__ . "/../config/{$this->package}.php" => config_path("{$this->package}.php"),
        ]);
    }

    public function provides(): array
    {
        return [
            NoCaptchaContract::class,
            NoCaptchaManagerContract::class,
        ];
    }

    private function registerNoCaptchaManager(): void
    {
        $this->app->singleton(NoCaptchaManagerContract::class, NoCaptchaManager::class);

        $this->app->bind(NoCaptchaContract::class, function (Application $app) {
            /** @var  Repository $config */
            $config = $app['config'];

            return $app->make(NoCaptchaManagerContract::class)->version(
                $config->get("$this->package.version"),
            );
        });
    }
}
