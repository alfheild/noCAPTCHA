<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha;

use Arcanedev\NoCaptcha\Elements\Input;
use Arcanedev\NoCaptcha\Utilities\AbstractResponse;
use Arcanedev\NoCaptcha\Utilities\ResponseV3;
use Illuminate\Support\HtmlString;

class NoCaptchaV3 extends AbstractNoCaptcha
{
    protected bool $scriptLoaded = false;

    public function input(string $name = 'g-recaptcha-response'): Input
    {
        return Input::create()
            ->setAttribute('type', 'hidden')
            ->setAttribute('id', $name)
            ->setAttribute('name', $name);
    }

    public function script(?string $callbackName = null): HtmlString
    {
        $script = '';

        if (!$this->scriptLoaded) {
            $script = implode(PHP_EOL, [
                '<script src="' . $this->getScriptSrc($callbackName) . '"></script>',
            ]);
            $this->scriptLoaded = true;
        }

        return new HtmlString($script);
    }

    public function getApiScript(): HtmlString
    {
        return new HtmlString(
            "<script>
                window.noCaptcha = {
                    render: function(action, callback) {
                        grecaptcha.execute('" . $this->getSiteKey() . "', {action})
                              .then(callback);
                    }
                }
            </script>",
        );
    }

    private function hasCallbackName($callbackName): bool
    {
        return !(is_null($callbackName) || trim($callbackName) === '');
    }

    protected function parseResponse(string $json): AbstractResponse
    {
        return ResponseV3::fromJson($json);
    }

    private function getScriptSrc(?string $callbackName = null): string
    {
        $queries = [];

        if ($this->hasLang()) {
            $queries['hl'] = $this->getLang();
        }

        $queries['render'] = $this->getSiteKey();

        if ($this->hasCallbackName($callbackName)) {
            $queries['onload'] = $callbackName;
        }

        return self::getClientUrl() . (count($queries) ? '?' . http_build_query($queries) : '');
    }
}
