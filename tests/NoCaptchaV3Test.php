<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Tests;

use Arcanedev\NoCaptcha\Exceptions\ApiException;
use Arcanedev\NoCaptcha\NoCaptchaV3;
use Arcanedev\NoCaptcha\Utilities\CaptchaRequest;
use PHPUnit\Framework\Attributes\Test;
use Prophecy\Argument;
use Psr\Http\Message\ServerRequestInterface;

final class NoCaptchaV3Test extends TestCase
{
    #[Test]
    public function it_can_get_client_url(): void
    {
        static::assertSame(
            'https://www.google.com/recaptcha/api.js',
            NoCaptchaV3::getClientUrl(),
        );

        NoCaptchaV3::$useGlobalDomain = true;

        static::assertSame(
            'https://www.recaptcha.net/recaptcha/api.js',
            NoCaptchaV3::getClientUrl(),
        );

        NoCaptchaV3::$useGlobalDomain = false;

        static::assertSame(
            'https://www.google.com/recaptcha/api.js',
            NoCaptchaV3::getClientUrl(),
        );
    }

    #[Test]
    public function it_can_get_verification_url(): void
    {
        static::assertSame(
            'https://www.google.com/recaptcha/api/siteverify',
            NoCaptchaV3::getVerificationUrl(),
        );

        NoCaptchaV3::$useGlobalDomain = true;

        static::assertSame(
            'https://www.recaptcha.net/recaptcha/api/siteverify',
            NoCaptchaV3::getVerificationUrl(),
        );

        NoCaptchaV3::$useGlobalDomain = false;

        static::assertSame(
            'https://www.google.com/recaptcha/api/siteverify',
            NoCaptchaV3::getVerificationUrl(),
        );
    }

    #[Test]
    public function it_can_be_instantiated(): void
    {
        $noCaptcha = $this->createCaptcha();

        static::assertInstanceOf(NoCaptchaV3::class, $noCaptcha);
        static::assertSame(
            '<script src="https://www.google.com/recaptcha/api.js?render=site-key"></script>',
            $noCaptcha->script()->toHtml(),
        );
    }

    #[Test]
    public function it_can_be_instantiated_with_defaults(): void
    {
        $noCaptcha = $this->createCaptcha();

        static::assertSame(
            '<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">',
            $noCaptcha->input()->toHtml(),
        );
    }

    #[Test]
    public function it_must_throw_api_exception_on_empty_secret_key(): void
    {
        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('Config secret key must not be empty');

        $this->createCaptcha('   ', 'key');
    }

    #[Test]
    public function it_must_throw_api_exception_on_empty_site_key(): void
    {
        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('Config site key must not be empty');

        $this->createCaptcha('secret', '   ');
    }

    #[Test]
    public function it_can_switch_locale(): void
    {
        $noCaptcha = $this->createCaptcha()->setLang('fr');

        static::assertInstanceOf(NoCaptchaV3::class, $noCaptcha);
        static::assertSame(
            '<script src="https://www.google.com/recaptcha/api.js?hl=fr&render=site-key"></script>',
            $noCaptcha->script()->toHtml(),
        );
    }

    #[Test]
    public function it_can_render_script_tag(): void
    {
        $noCaptcha = $this->createCaptcha();

        static::assertSame(
            '<script src="https://www.google.com/recaptcha/api.js?render=site-key"></script>',
            $noCaptcha->script()->toHtml(),
        );

        // Echo out only once
        static::assertEmpty($noCaptcha->script()->toHtml());
    }

    #[Test]
    public function it_can_render_script_tag_with_lang(): void
    {
        $noCaptcha = $this->createCaptcha()->setLang('fr');

        static::assertSame(
            '<script src="https://www.google.com/recaptcha/api.js?hl=fr&render=site-key"></script>',
            $noCaptcha->script()->toHtml(),
        );

        // Not twice
        static::assertEmpty($noCaptcha->script()->toHtml());
    }

    #[Test]
    public function it_can_render_script_tag_with_onload_callback(): void
    {
        $noCaptcha = $this->createCaptcha();

        static::assertSame(
            '<script src="https://www.google.com/recaptcha/api.js?render=site-key&onload=no_captcha_onload"></script>',
            $noCaptcha->script('no_captcha_onload')->toHtml(),
        );

        // Not twice
        static::assertEmpty($noCaptcha->script()->toHtml());
    }

    #[Test]
    public function it_can_render_api_script(): void
    {
        $noCaptcha = $this->createCaptcha();

        static::assertSame(
            "<script>
                window.noCaptcha = {
                    render: function(action, callback) {
                        grecaptcha.execute('site-key', {action})
                              .then(callback);
                    }
                }
            </script>",
            $noCaptcha->getApiScript()->toHtml(),
        );
    }

    #[Test]
    public function it_can_verify(): void
    {
        $noCaptcha = $this->createCaptcha();

        /** @var CaptchaRequest $request */
        $request = tap($this->prophesize(CaptchaRequest::class), function ($request) {
            $request->send(Argument::type('string'))->willReturn('{"success": true}');
        })->reveal();

        $response = $noCaptcha
            ->setRequestClient($request)
            ->verify('re-captcha-response');

        static::assertTrue($response->isSuccessful());
    }

    #[Test]
    public function it_can_verify_psr7_request(): void
    {
        /** @var  CaptchaRequest $client */
        $client = tap($this->prophesize(CaptchaRequest::class), function ($client) {
            $client->send(Argument::type('string'))->willReturn(
                '{"success": true}',
            );
        })->reveal();

        /** * @var  \Psr\Http\Message\ServerRequestInterface $request */
        $request = tap($this->prophesize(ServerRequestInterface::class), function ($request) {
            $request->getParsedBody()->willReturn([
                'g-recaptcha-response' => true,
            ]);
            $request->getServerParams()->willReturn([
                'REMOTE_ADDR' => '127.0.0.1',
            ]);
        })->reveal();

        $response = $this->createCaptcha()
            ->setRequestClient($client)
            ->verifyRequest($request);

        static::assertTrue($response->isSuccessful());
    }

    #[Test]
    public function it_can_verify_with_fails(): void
    {
        $noCaptcha = $this->createCaptcha();

        $response = $noCaptcha->verify('');

        static::assertFalse($response->isSuccessful());

        $request = tap($this->prophesize(CaptchaRequest::class), function ($request) {
            $request->send(Argument::type('string'))->willReturn(
                json_encode([
                    'success' => false,
                    'error-codes' => 'invalid-input-response',
                ]),
            );
        })->reveal();

        $response = $noCaptcha
            ->setRequestClient($request)
            ->verify('re-captcha-response');

        static::assertFalse($response->isSuccessful());
    }

    #[Test]
    public function it_can_render_captcha_with_optional_name(): void
    {
        $noCaptcha = $this->createCaptcha();

        static::assertSame(
            '<input type="hidden" id="g-recaptcha" name="g-recaptcha">',
            $noCaptcha->input('g-recaptcha')->toHtml(),
        );
    }

    private function createCaptcha(
        string $secret = 'secret-key',
        string $siteKey = 'site-key',
        ?string $lang = null,
    ): NoCaptchaV3 {
        return new NoCaptchaV3($secret, $siteKey, $lang);
    }
}
