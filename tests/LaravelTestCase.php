<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Tests;

use Arcanedev\NoCaptcha\NoCaptchaServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

abstract class LaravelTestCase extends BaseTestCase
{
    use ProphecyTrait;

    protected function getPackageProviders($app): array
    {
        return [
            NoCaptchaServiceProvider::class,
        ];
    }
}
