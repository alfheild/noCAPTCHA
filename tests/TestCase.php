<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Tests;

use PHPUnit\Framework\TestCase as BaseTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

abstract class TestCase extends BaseTestCase
{
    use ProphecyTrait;
}
