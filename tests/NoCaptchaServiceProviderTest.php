<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Tests;

use Arcanedev\NoCaptcha\Contracts\NoCaptcha as NoCaptchaInterface;
use Arcanedev\NoCaptcha\Contracts\NoCaptchaManager as NoCaptchaManagerInterface;
use Arcanedev\NoCaptcha\NoCaptchaServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use PHPUnit\Framework\Attributes\Test;

final class NoCaptchaServiceProviderTest extends LaravelTestCase
{
    private NoCaptchaServiceProvider|ServiceProvider $provider;

    public function setUp(): void
    {
        parent::setUp();

        $this->provider = $this->app->getProvider(NoCaptchaServiceProvider::class);
    }

    public function tearDown(): void
    {
        unset($this->provider);

        parent::tearDown();
    }

    #[Test]
    public function it_can_be_instantiated(): void
    {
        $expectations = [
            ServiceProvider::class,
            DeferrableProvider::class,
            NoCaptchaServiceProvider::class,
        ];

        foreach ($expectations as $expected) {
            static::assertInstanceOf($expected, $this->provider);
        }
    }

    #[Test]
    public function it_can_provides(): void
    {
        $expected = [
            NoCaptchaInterface::class,
            NoCaptchaManagerInterface::class,
        ];

        static::assertSame($expected, $this->provider->provides());
    }
}
