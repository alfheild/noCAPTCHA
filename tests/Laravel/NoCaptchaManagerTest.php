<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Tests\Laravel;

use Arcanedev\NoCaptcha\Contracts\NoCaptchaManager as NoCaptchaManagerInterface;
use Arcanedev\NoCaptcha\NoCaptchaManager;
use Arcanedev\NoCaptcha\NoCaptchaV2;
use Arcanedev\NoCaptcha\NoCaptchaV3;
use Arcanedev\NoCaptcha\Tests\LaravelTestCase;
use Illuminate\Support\Manager;
use PHPUnit\Framework\Attributes\Test;

final class NoCaptchaManagerTest extends LaravelTestCase
{
    protected NoCaptchaManagerInterface $manager;

    protected function setUp(): void
    {
        parent::setUp();

        $this->manager = $this->app->make(NoCaptchaManagerInterface::class);
    }

    protected function tearDown(): void
    {
        unset($this->manager);

        parent::tearDown();
    }

    #[Test]
    public function it_can_be_instantiated(): void
    {
        $expectations = [
            Manager::class,
            NoCaptchaManagerInterface::class,
            NoCaptchaManager::class,
        ];

        foreach ($expectations as $expected) {
            static::assertInstanceOf($expected, $this->manager);
        }
    }

    #[Test]
    public function it_can_get_default_driver(): void
    {
        static::assertInstanceOf(
            NoCaptchaV3::class,
            $this->manager->version(),
        );
    }

    #[Test]
    public function it_can_get_default_driver_via_helper(): void
    {
        static::assertInstanceOf(
            NoCaptchaV3::class,
            no_captcha(),
        );
    }

    #[Test]
    public function it_can_get_driver_by_given_version(): void
    {
        $versions = [
            'v3' => NoCaptchaV3::class,
            'v2' => NoCaptchaV2::class,
        ];

        foreach ($versions as $version => $class) {
            $captcha = $this->manager->version();

            static::assertInstanceOf(NoCaptchaV3::class, $captcha);
        }
    }

    #[Test]
    public function it_must_throw_exception_on_unsupported_version(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Driver [v1] not supported.');

        $this->manager->version('v1');
    }

    #[Test]
    public function it_can_set_lang_from_locale(): void
    {
        $versions = [
            'v3' => NoCaptchaV3::class,
            'v2' => NoCaptchaV2::class,
        ];

        $this->app->setLocale('fr');

        foreach ($versions as $version => $class) {
            $captcha = $this->manager->version($version);

            static::assertSame('fr', $captcha->getLang());
        }
    }
}
