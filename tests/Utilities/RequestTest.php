<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Tests\Utilities;

use Arcanedev\NoCaptcha\Contracts\CaptchaRequest as CaptchaRequestInterface;
use Arcanedev\NoCaptcha\Exceptions\InvalidUrlException;
use Arcanedev\NoCaptcha\Tests\TestCase;
use Arcanedev\NoCaptcha\Utilities\CaptchaRequest;
use PHPUnit\Framework\Attributes\Test;

final class RequestTest extends TestCase
{
    private const URL_TO_CURL_OR_WHATEVER = 'http://httpbin.org/get';

    /** @var CaptchaRequest */
    private CaptchaRequest $request;

    public function setUp(): void
    {
        parent::setUp();

        $this->request = new CaptchaRequest();
    }

    public function tearDown(): void
    {
        unset($this->request);

        parent::tearDown();
    }

    #[Test]
    public function it_can_be_instantiated(): void
    {
        $expectations = [
            CaptchaRequestInterface::class,
            CaptchaRequest::class,
        ];

        foreach ($expectations as $expected) {
            static::assertInstanceOf($expected, $this->request);
        }
    }

    #[Test]
    public function it_must_throw_api_exception_on_url(): void
    {
        $this->expectException(InvalidUrlException::class);
        $this->expectExceptionMessage('The url must not be empty');

        $this->request->send('');
    }

    #[Test]
    public function it_must_throw_invalid_url_exception_on_url(): void
    {
        $this->expectException(InvalidUrlException::class);
        $this->expectExceptionMessage('The url [trust-me-im-a-valid-url] is invalid');

        $this->request->send('trust-me-im-a-valid-url');
    }
}
