<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Tests\Utilities;

use Arcanedev\NoCaptcha\Tests\TestCase;
use Arcanedev\NoCaptcha\Utilities\ResponseV2;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use JsonSerializable;
use PHPUnit\Framework\Attributes\Test;

final class ResponseV2Test extends TestCase
{
    #[Test]
    public function it_can_be_instantiated(): void
    {
        $response = new ResponseV2(true);

        static::assertTrue($response->isSuccessful());
    }

    #[Test]
    public function it_can_be_instantiated_from_json(): void
    {
        $response = ResponseV2::fromJson('{"success": true}');

        static::assertTrue($response->isSuccessful());
    }

    #[Test]
    public function it_can_convert_to_json(): void
    {
        $response = ResponseV2::fromArray(['success' => true]);

        static::assertInstanceOf(JsonSerializable::class, $response);
        static::assertInstanceOf(Jsonable::class, $response);

        static::assertSame(
            '{"success":true,"hostname":null,"challenge_ts":null,"apk_package_name":null,"error-codes":[]}',
            $response->toJson()
        );
    }

    #[Test]
    public function it_can_be_instantiated_from_array(): void
    {
        $response = ResponseV2::fromArray(['success' => true]);

        static::assertTrue($response->isSuccessful());
    }

    #[Test]
    public function it_can_get_response_data(): void
    {
        $response = new ResponseV2(
            false,
            ['challenge-timeout'],
            'localhost',
            '2019-01-01T00:00:00Z',
            null,
            0.5,
            'action-name'
        );

        static::assertFalse($response->isSuccessful());
        static::assertSame(['challenge-timeout'], $response->getErrorCodes());
        static::assertSame('localhost', $response->getHostname());
        static::assertSame('2019-01-01T00:00:00Z', $response->getChallengeTs());
        static::assertNull($response->getApkPackageName());

        static::assertEquals([
            'success'          => false,
            'hostname'         => 'localhost',
            'challenge_ts'     => '2019-01-01T00:00:00Z',
            'apk_package_name' => null,
            'error-codes'      => ['challenge-timeout'],
        ], $response->toArray());
    }

    #[Test]
    public function it_can_convert_to_array(): void
    {
        $response = new ResponseV2(
            false,
            ['challenge-timeout'],
            'localhost',
            '2019-01-01T00:00:00Z',
            null
        );

        static::assertInstanceOf(Arrayable::class, $response);
        static::assertEquals([
            'success'          => false,
            'hostname'         => 'localhost',
            'challenge_ts'     => '2019-01-01T00:00:00Z',
            'apk_package_name' => null,
            'error-codes'      => ['challenge-timeout'],
        ], $response->toArray());
    }

    #[Test]
    public function it_can_check_the_hostname(): void
    {
        $response = ResponseV2::fromArray([
            'success'  => true,
            'hostname' => 'localhost',
        ]);

        static::assertTrue($response->isHostname('localhost'));

        $response = ResponseV2::fromArray([
            'success'  => true,
            'hostname' => 'example.com',
        ]);

        static::assertFalse($response->isHostname('localhost'));
    }
}
