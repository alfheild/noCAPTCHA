<?php

declare(strict_types=1);

namespace Arcanedev\NoCaptcha\Tests\Utilities;

use Arcanedev\NoCaptcha\Tests\TestCase;
use Arcanedev\NoCaptcha\Utilities\ResponseV3;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use JsonSerializable;
use PHPUnit\Framework\Attributes\Test;

final class ResponseV3Test extends TestCase
{
    #[Test]
    public function it_can_be_instantiated(): void
    {
        $response = new ResponseV3(true);

        static::assertTrue($response->isSuccessful());
    }

    #[Test]
    public function it_can_be_instantiated_from_json(): void
    {
        $response = ResponseV3::fromJson('{"success": true}');

        static::assertTrue($response->isSuccessful());
    }

    #[Test]
    public function it_can_be_instantiated_with_invalid_json(): void
    {
        $response = ResponseV3::fromJson('');

        static::assertFalse($response->isSuccessful());
        static::assertEquals([ResponseV3::E_INVALID_JSON], $response->getErrorCodes());
    }

    #[Test]
    public function it_can_convert_to_json(): void
    {
        $response = ResponseV3::fromArray(['success' => true]);

        static::assertInstanceOf(JsonSerializable::class, $response);
        static::assertInstanceOf(Jsonable::class, $response);

        static::assertSame(
            '{"success":true,"hostname":null,"challenge_ts":null,"apk_package_name":null,"score":null,"action":null,"error-codes":[]}',
            $response->toJson()
        );
    }

    #[Test]
    public function it_can_be_instantiated_from_array(): void
    {
        $response = ResponseV3::fromArray(['success' => true]);

        static::assertTrue($response->isSuccessful());
    }

    #[Test]
    public function it_can_get_response_data(): void
    {
        $response = new ResponseV3(
            false,
            ['challenge-timeout'],
            'localhost',
            '2019-01-01T00:00:00Z',
            null,
            0.5,
            'action-name'
        );

        static::assertFalse($response->isSuccessful());
        static::assertSame(['challenge-timeout'], $response->getErrorCodes());
        static::assertSame('localhost', $response->getHostname());
        static::assertSame('2019-01-01T00:00:00Z', $response->getChallengeTs());
        static::assertNull($response->getApkPackageName());
        static::assertSame(0.5, $response->getScore());
        static::assertSame('action-name', $response->getAction());

        static::assertEquals([
            'success'          => false,
            'hostname'         => 'localhost',
            'challenge_ts'     => '2019-01-01T00:00:00Z',
            'apk_package_name' => null,
            'score'            => 0.5,
            'action'           => 'action-name',
            'error-codes'      => ['challenge-timeout'],
        ], $response->toArray());
    }

    #[Test]
    public function it_can_convert_to_array(): void
    {
        $response = new ResponseV3(
            false,
            ['challenge-timeout'],
            'localhost',
            '2019-01-01T00:00:00Z',
            null,
            0.5,
            'action-name'
        );

        static::assertInstanceOf(Arrayable::class, $response);
        static::assertEquals([
            'success'          => false,
            'hostname'         => 'localhost',
            'challenge_ts'     => '2019-01-01T00:00:00Z',
            'apk_package_name' => null,
            'score'            => 0.5,
            'action'           => 'action-name',
            'error-codes'      => ['challenge-timeout'],
        ], $response->toArray());
    }

    #[Test]
    public function it_can_check_the_hostname(): void
    {
        $response = ResponseV3::fromArray([
            'success'  => true,
            'hostname' => 'localhost',
        ]);

        static::assertTrue($response->isHostname('localhost'));

        $response = ResponseV3::fromArray([
            'success'  => true,
            'hostname' => 'example.com',
        ]);

        static::assertFalse($response->isHostname('localhost'));
    }
}
